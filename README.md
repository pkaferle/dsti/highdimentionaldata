# HighDimentionalData

Exploration of Paris AirBnB data. 

Methods:
- data cleanning
- variable selection
- PCA
- hierarchical cluatering
- k-means clustering
- visualisation

![Capture_Map](/uploads/2a627d21d9bf00f58e0b513b144d69ea/Capture_Map.PNG)
